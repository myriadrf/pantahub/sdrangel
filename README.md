# Readme SDR Angel

## SDR Angel for Pantahub

See:

* https://www.pantahub.com
* https://docs.pantahub.com

To install you go through four steps:

 1. Clone your device from pantahub
 1. Remove other SDR using apps with pvr
 1. Add/Commit SDR Angel app with pvr
 1. Post changes to your device

### Clone your device from pantahub

Assuming your username is "user1" and your device name in pantahub is "device1", use the following:

```
pvr clone user1/device1 to-local-dir
```

This will put a copy of the device state in directory ```to-local-dir```.

Before moving to next step ```cd``` into that directory:

```
cd to-local-dir
```

## Remove other apps you previously installed:

First you can check what apps and platforms you have installed using pvr:

```
pvr app ls
```

Note that some of these are essential and others might not cause any conflict with sdrangel. So try to touch only those that you added in the past and that are using the LimeSDR.

To remove an app you use:

```
pvr app rm <app-name>
pvr commit
```

For example:
```
pvr app rm some-other-app
pvr commit
```

## Add/Commit SDR Angel app with pvr

```
pvr app add --from registry.gitlab.com/myriadrf/pantahub/sdrangel:ARMHF sdr-angel
```

You can confirm that a new folder called "sdr-angel" got added by using the pvr:

```
pvr status
[...]
? sdr-angel/lxc.container.conf
? sdr-angel/root.squashfs
? sdr-angel/root.squashfs.docker-digest
? sdr-angel/run.json
? sdr-angel/src.json
```

If you are happy with the result (by default: yes), go ahead and add  it locally:

```
pvr add .
```


... and the commit it to local repo:

```
pvr commit
Adding sdr-angel/lxc.container.conf
Adding sdr-angel/root.squashfs
Adding sdr-angel/root.squashfs.docker-digest
Adding sdr-angel/run.json
Adding sdr-angel/src.json
```

### Post changes to your device

With your new device state assembled and committed *locally*, you can now move on to next step and post the device revision to your device


```
pvr post -m "add sdr-angel"
```

This command will deploy the locally assembled state to the remove device you cloned the repo from.

You can of course also push to any other device you have in your account:

```
pvr post -m "rollout with sdr-angel" user1/deviceX
```


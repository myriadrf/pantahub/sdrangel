#!/bin/bash
service ssh start
service dbus start
service avahi-daemon start
/usr/bin/pulseaudio -D --system --realtime --disallow-exit --no-cpu-limit
http-server /www &
sleep 2
IPADDR=$(ip addr show eth0 | grep -oE "\b([0-9]{1,3}\.){3}[0-9]{1,3}\b" | head -1)
export LD_LIBRARY_PATH=/opt/install/libsdrplay/lib:/opt/install/xtrx-images/lib
echo "ondemand" > /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor
/opt/install/sdrangel/bin/sdrangelsrv -a ${IPADDR} 2>&1 | tee -a /pvstorage/sdrangel-log | /usr/bin/pvlogger stdin
